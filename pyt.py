#!/usr/bin/env python3

import subprocess
from random import uniform, randint, random
from sys import argv

TIMEOUT = 180

if "--log" in argv:
    print("log")

    with open("log.log", "w") as f:
        pass #clear

    for x in range(int(argv[2])):
        x, n = uniform(-100, 1000000), randint(0, 100000000)
        cmd = ['time ./proj2.out --log ' + str(x) + ' ' + str(n)]

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        with open("log.log", "a") as f:
            f.write(str(cmd)+"\n")
            for l in p.stdout:
                f.write(l.decode("utf-8"))
            for l in p.stderr:
                f.write(">>>" + l.decode("utf-8"))

            f.write("\n\n")
        p.wait(timeout=TIMEOUT)
        print(p.returncode)

elif "--iter" in argv:
    print("iter")

    with open("iter.log", "w") as f:
        pass #clear

    for x in range(int(argv[2])):
        min, max, eps = uniform(-100, 1000000), uniform(-100, 1000000), uniform(0, 0.001)
        cmd = ['time ./proj2.out --iter ' + str(min) + ' ' + str(max) + ' ' + str(eps)]

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        with open("iter.log", "a") as f:
            f.write(str(cmd)+"\n")
            for l in p.stdout:
                f.write(l.decode("utf-8"))
            for l in p.stderr:
                f.write(">>>" + l.decode("utf-8"))

            f.write("\n\n")
        p.wait(timeout=TIMEOUT)
        print(p.returncode)
