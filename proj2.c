/**
 * @file proj2.c
 *
 * IZP Project 02
 *
 * @author    Adrian Kiraly (xkiral01@stud.fit.vutbr.cz) [1BIA]
 * @date      2015-11-10
 * @version   1.1
 *
 * Calculation of natural logarithm using Taylor series and continued fractions.
 * More info on usage can be obtained by running compiled binary with no
 * arguments (i.e. ./proj2)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include <assert.h>

typedef unsigned long ulong;

// Debugging
// =============================================================================

#if DEBUG
/// Formated logger
#define flog(M, ...) fprintf(stderr, "%s:%d:%s: " M "\n", __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
/// Simple logger (without variadic arguments)
#define slog(M) flog("%s", M)
#else
/// Disable flog
#define flog(M, ...)
/// Disable slog
#define slog(M)
#endif

// Misc macros (aka magic!)
// =============================================================================

/// Floating point number equivalency
#define FEQV(a, eps) (fabs(a) <= (eps))

/// Get greater of 2 variables
#define MAX(a, b) ((a) < (b)) ? (b) : (a)

/// Compare argc with x, return error if argc not greater than x or equal
/// @todo do we want magic?
#define ARGC_GTE_OR_DIE(x) if (argc < x) return die_with_error(ERR_MISSING_ARG, "")

// Constants
// =============================================================================

/// Threshold for polynomials used by Taylor series
#define TAYLOG_THRESHOLD 1

// Enums
// =============================================================================

/**
 * @brief    Argument options
 *
 * Used for mapping of strings for argument comparison to enums
 */
typedef enum {
    /// --log
    ARG_LOG,
    /// --iter
    ARG_ITER
} TArgument;


/**
 * @brief    Error codes
 *
 * Return codes for errors
 */
typedef enum {
    /// Missing paramater
    ERR_NOPAR,
    /// Illegal option
    ERR_ILLEGAL_OPT,
    /// Missing arguments
    ERR_MISSING_ARG,
    /// Invalid argument
    ERR_INVALID_ARG
} TErrCodes;


/**
 * @brief    Output messages
 *
 * Used for localization strings for output
 */
typedef enum {
    OM_LOG,
    OM_CFRAC,
    OM_TAYLOG,
    OM_IT_CFRAC,
    OM_IT_TAYLOG
} TOutMessages;

// Localization strings
// =============================================================================

/// Strings for argument comparison
const char *LOC_ARGUMENT[] = {
    [ARG_LOG] = "--log",
    [ARG_ITER] = "--iter"
};

/// Localized strings for error messages
const char *LOC_ERRCODES[] = {
    [ERR_ILLEGAL_OPT] = "illegal option %s",
    [ERR_MISSING_ARG] = "missing arguments %s",
    [ERR_INVALID_ARG] = "invalid argument %s"
};

/// Localized strings for output messages
const char *LOC_OUTMESSAGES[] = {
    [OM_LOG] = "log",
    [OM_CFRAC] = "cf_log",
    [OM_TAYLOG] = "taylor_log",
    [OM_IT_CFRAC] = "continued fraction iterations",
    [OM_IT_TAYLOG] = "taylor polynomial iterations"
};

// Utility functions
// =============================================================================

/**
 * @brief      Compare string with an argument type
 *
 * Comapres argument with a argument type specified by `TArgument`
 *
 * @param[in]  arg      String to comapre
 * @param[in]  argtype  Argument type
 *
 * @return     `true` if `arg` equals argument type, otherwise `false`
 */
bool argcmp(const char *arg, TArgument argtype) {
    return (strcmp(arg, LOC_ARGUMENT[argtype]) == 0);
}


/**
 * @brief      Facilities for calculation of natural logarithms using Taylor series
 *
 * @note
 * This function provides functionality for wrapper functions taylor_log() and
 * iterations_taylor() and thus should never be called directly.
 *
 * If called with `*iter` set to `NULL`, `orig` and `teps` arguments are ignored
 * and logarithm will be calculated with `n` iterations.
 *
 * If called with `*iter` set to a pointer for unsigned long, specified number
 * of iterations is ignored and logarithm is calculated until accuracy specified
 * by `teps` is reached. Number of required iterations is then saved to `*iter`.
 *
 * @param[in]  x     Value (mandatory)
 * @param[in]  n     Number of iterations (ignored if `iter` is `NULL`)
 * @param      iter  Pointer to integer or `NULL`
 * @param[in]  orig  Original value for comparison (ignored if `iter` is not `NULL`)
 * @param[in]  teps  Target epsilon (ignored if `iter` is not `NULL`)
 *
 * @return     Natural logarithm of `x` after `n` iterations or with accuracy
 *             higher than or equal to accuracy specified by `teps`
 */
double _calc_taylog(double x, ulong n, ulong *iter, double orig, double teps) {
    // Logarithm is undefined for nonpositive numbers
    if (x <= 0) {
        return (x == 0) ? -INFINITY : NAN;
    }

    if (isinf(x)) {return INFINITY;}
    if (isnan(x)) {return NAN;}

    double sum = 0;
    double psum = 0;

    bool poly_1 = (x < TAYLOG_THRESHOLD);   // Use first polynomial?
    bool search_iter = (iter != NULL);      // Search for number of iterations?

    // Change nominator according to used polynomial
    if (poly_1) {
        x = 1 - x;
    }
    else {
        x = (x - 1) / x;
    }

    double nom = x;

    for (ulong i = 1; i <= n || search_iter; ++i) { // Infinite cycle if search
        psum = sum;
        sum += nom / i;

        flog("poly=%d\titer=%lu\tnom=%g\tsum=%.12g", !poly_1+1, i, nom, sum);

        if (search_iter) {
            *iter = i;
        }

        if (psum == sum) {
            // No point going further
            flog("Reached machine eps after %lu iterations, breaking", i);
            break;
        }

        if (search_iter && FEQV(orig - (poly_1 ? -sum : sum), teps)) {
            flog("Reached target epsilon after %lu iterations", i);
            break;
        }

        nom *= x;
    }

    // Multiply by -1 if used the first polynomial
    return poly_1 ? -sum : sum;
}


/**
 * @brief      Print error message and return error code
 *
 * Prints error message specified by the error code. Up to one string of
 * additional information can be added to the message. If the specified message
 * doesn't support additional info, an empty string should be passed.
 *
 * @note
 * This function only makes sense when used like this:
 * `return die_with_error();`
 *
 * @param[in]  err   Error code
 * @param[in]  info  Additional information
 *
 * @return     Error code passed in `err`
 */
int die_with_error(TErrCodes err, const char *info) {
    printf("error: ");
    printf(LOC_ERRCODES[err], info);
    printf("\n");
    return err;
}


// Functions
// =============================================================================

/**
 * @brief      Calculate natural logarithm of given value using Taylor series
 *
 * @param[in]  x     Value
 * @param[in]  n     Number of iterations
 *
 * @return     Natural logarithm of `x` after `n` iterations
 */
double taylor_log(double x, ulong n) {
    return _calc_taylog(x, n, NULL, 0, 0);
}


/**
 * @brief      Calculate natural logarithm of given value using continued
 *             fractions
 *
 * @param[in]  x     Value
 * @param[in]  n     Number of iterations
 *
 * @return     Natural logarithm of `x` after `n` iterations
 */
double cfrac_log(double x, ulong n) {
    // Logarithm is undefined for nonpositive numbers
    if (x <= 0) {
        return (x == 0) ? -INFINITY : NAN;
    }

    if (isinf(x)) {return INFINITY;}
    if (isnan(x)) {return NAN;}

    if (n == 0) {
        return 0;
    }

    double sum = 0;

    const double z = (x-1) / (x+1);
    const double zsq = z * z;

    for (ulong i = n-1; i > 0; --i) {
        sum = (i * (i*zsq)) / ((2*i + 1) - sum);
        flog("iter=%lu\tnnom=%luz^2\tnom=%g\tdenom=%lu-sum\tsum=%g", i, i*i, (i*(i*zsq)), (2*i + 1), sum);
    }

    return (2*z) / (1-sum);
}


/**
 * @brief      Calculate number of required iterations needed for specified
 *             accuracy using Taylor series
 *
 * Calculates number of iterations needed until the difference between `math.h`'s
 * log() and taylor_log() is less or equal to `eps`
 *
 * @note
 * Implemented using simple for cycle, as binary search on Taylor series is
 * wildly inefficient
 *
 * @param[in]  x     Value
 * @param[in]  eps   Epsilon
 *
 * @return     Number of required iterations
 */
ulong iterations_taylor(double x, double eps) {
    ulong result = 0;
    _calc_taylog(x, 0, &result, log(x), eps);
    return result;
}


/**
 * @brief      Calculate number of required iterations needed for specified
 *             accuracy using continued fractions
 *
 * Calculates number of iterations needed until the difference between `math.h`'s
 * log() and cfrac_log() is less or equal to `eps`
 *
 * @note
 * This function uses binary search and requires specifing upper limit on
 * number of iterations. It's recommended to have at least somewhat reasonable
 * approximation of this value for best efficiency of search.
 *
 * @param[in]  x     Value
 * @param[in]  eps   Epsilon
 * @param[in]  imax  Upper limit on iterations for binary search
 *
 * @return     Number of required iterations
 */
ulong iterations_cfrac(double x, double eps, ulong imax) {
    if (x <= 0 || imax == 0) {
        return 0;
    }

    ulong imin = 1;
    double orig = log(x); slog("hello");

    while (imin < imax) {
        ulong imid = (imax-imin)/2 + imin;

        flog("imin=%lu\timid=%lu\timax=%lu", imin, imid, imax);

        double cfrac = cfrac_log(x, imid);

        if (isnan(cfrac) || isinf(cfrac)) {
            return 0;
        }

        if (FEQV(orig - cfrac, eps)) {
            imax = imid;
            slog("imax=imid");
        }
        else {
            imin = imid + 1;
            slog("imin=imid+1");
        }
    }

    // Fix number of iterations
    if (!FEQV(cfrac_log(x, imin) - orig, eps)) {
        slog("bumping iter to reach target eps");
        imin++;
    }

    /// @todo remove this maybe
    double calc = cfrac_log(x, imin);
    flog("%lu iterations, eps=%.12g", imin, fabs(orig - calc));
    assert(FEQV(orig - calc, eps)); slog("ok");
    return imin;
}


// Output functions
// =============================================================================

/**
 * @brief      Print help message
 */
void print_help(const char *binary_name) {
    printf(
    "usage:\n\n"
    "%s --log [x] [n]\n"
    "    Calculates natural logarithm of x using Taylor series and continued\n"
    "    fractions with n iterations\n\n"
    "%s --iter [min] [max] [eps]\n"
    "    Calculates number of iterations required for calculating natural\n"
    "    logarithm using Taylor series and continued fractions in the\n"
    "    interval [min, max] while maintaining the difference between real\n"
    "    and calculated values smaller than or equal to eps.\n"
    "-----------------------------------------------------------------------\n"
    "IZP Project 02 / Iterative calculations\n\n"
    "version 1.1                                        Adrian Kiraly [1BIA]\n"
    "compiled on: %s %s            xkiral01@stud.fit.vutbr.cz\n",
    binary_name, binary_name, __DATE__, __TIME__);
}


/**
 * @brief      Print formated string containing number of iterations
 *
 * @param[in]  func_type  Method of logarithm calculation
 * @param[in]  i          Number of iteration
 */
void print_iterations(TOutMessages func_type, ulong i) {
    printf("%s = %lu\n", LOC_OUTMESSAGES[func_type], i);
}


/**
 * @brief      Print formated string containing result of calculation
 *
 * @param[in]  func_type  Method of logarithm calculation
 * @param[in]  value      Value
 * @param[in]  result     Result of calculation
 */
void print_result(TOutMessages func_type, double value, double result) {
    printf("%10s(%g) = %.12g\n", LOC_OUTMESSAGES[func_type], value, result);
}


// Main function
// =============================================================================

/**
 * @brief      Main function
 *
 * @param[in]  argc  Argument count
 * @param[in]  argv  Argument vector (unused)
 *
 * @return     Return code
 */
int main(int argc, char const *argv[]) {
    // Argument processing

    if (argc >= 2) {
        if (argcmp(argv[1], ARG_LOG)) {
            ARGC_GTE_OR_DIE(4);

            // Parse input and check for errors from strtod and strtol
            char *end = NULL;

            double x = strtod(argv[2], &end);
            if (*end != 0) return die_with_error(ERR_INVALID_ARG, argv[2]);

            ulong n = strtoul(argv[3], &end, 10);
            if (*end != 0) return die_with_error(ERR_INVALID_ARG, argv[3]);

            print_result(OM_LOG, x, log(x));
            print_result(OM_CFRAC, x, cfrac_log(x, n));
            print_result(OM_TAYLOG, x, taylor_log(x, n));

            return EXIT_SUCCESS;
        }

        if (argcmp(argv[1], ARG_ITER)) {
            ARGC_GTE_OR_DIE(5);

            // Parse input and check for errors from strtod and strtol
            char *end = NULL;

            double min = strtod(argv[2], &end);
            if (*end != 0) return die_with_error(ERR_INVALID_ARG, argv[2]);

            double max = strtod(argv[3], &end);
            if (*end != 0) return die_with_error(ERR_INVALID_ARG, argv[3]);

            double eps = strtod(argv[4], &end);
            if (*end != 0) return die_with_error(ERR_INVALID_ARG, argv[4]);

            // Calculate min and max for taylor
            ulong taylor_min_i = iterations_taylor(min, eps);
            ulong taylor_max_i = iterations_taylor(max, eps);
            ulong taylor_i = MAX(taylor_min_i, taylor_max_i);

            // Calculate min and max for cfrac
            ulong cfrac_min_i = iterations_cfrac(min, eps, taylor_min_i);
            ulong cfrac_max_i = iterations_cfrac(max, eps, taylor_max_i);
            ulong cfrac_i = MAX(cfrac_min_i, cfrac_max_i);

            // Print output
            print_result(OM_LOG, min, log(min));
            print_result(OM_LOG, max, log(max));

            print_iterations(OM_IT_CFRAC, cfrac_i);
            print_result(OM_CFRAC, min, cfrac_log(min, cfrac_i));
            print_result(OM_CFRAC, max, cfrac_log(max, cfrac_i));

            print_iterations(OM_IT_TAYLOG, taylor_i);
            print_result(OM_TAYLOG, min, taylor_log(min, taylor_i));
            print_result(OM_TAYLOG, max, taylor_log(max, taylor_i));

            return EXIT_SUCCESS;
        }

        return die_with_error(ERR_ILLEGAL_OPT, argv[1]);
    }

    print_help(argv[0]);
    return ERR_NOPAR;
}
