CFLAGS=-Wall -Wextra -Werror -O2 -std=c99 -pedantic #-DDEBUG
LDFLAGS=-lm

MAIN=proj2
OUTFILE=$(MAIN).out

DOCS=doxygen
DOXYFILE=Doxyfile

$(OUTFILE): $(MAIN).c
	$(CC) $(CFLAGS) $(LDFLAGS) $(MAIN).c -o $(OUTFILE)
	$(DOCS) $(DOXYFILE) > /dev/null

.PHONY: clean cleandocs cleanall test

cleanall: clean cleandocs

clean:
	rm $(OUTFILE)

cleandocs:
	rm -rf html/ latex/

test: $(OUTFILE)
	#./runtests.sh ./$(OUTFILE)
